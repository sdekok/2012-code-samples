﻿using Assignment_3.Scene.Basic_Shapes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Assignment_3.GenericStructures;
using Assignment_3.Raytracer;

namespace Assignment_3_Tests
{
    
    
    /// <summary>
    ///This is a test class for SphereTest and is intended
    ///to contain all SphereTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SphereTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for getFirstIntersection
        ///</summary>
        [TestMethod()]
        public void getFirstIntersectionTest()
        {
            Vector center = new Vector(0,0,300); // TODO: Initialize to an appropriate value
            double radius = 150F; // TODO: Initialize to an appropriate value
            Sphere target = new Sphere(center, radius); // TODO: Initialize to an appropriate value
            Ray r = new Ray(new Vector(0,0,0), new Vector(0,0,1)); // TODO: Initialize to an appropriate value
            Vector expected = new Vector(0,0,150); // TODO: Initialize to an appropriate value
            Vector actual;
            actual = target.getFirstIntersection(r);
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.z, actual.z);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for getFirstIntersection
        ///</summary>
        [TestMethod()]
        public void getFirstIntersectionTest2()
        {
            Vector center = new Vector(0, 0, 300); // TODO: Initialize to an appropriate value
            double radius = 150F; // TODO: Initialize to an appropriate value
            Sphere target = new Sphere(center, radius); // TODO: Initialize to an appropriate value
            Ray r = new Ray(new Vector(0, 0, 451), new Vector(0, 0, 1)); // TODO: Initialize to an appropriate value
            //Vector expected = null; // TODO: Initialize to an appropriate value
            Vector actual;
            actual = target.getFirstIntersection(r);
            Assert.IsNull(actual);
            //Assert.AreEqual(expected.z, actual.z);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }


        /// <summary>
        ///A test for getFirstIntersection where the ray starts by the one object and ends near the second.
        ///</summary>
        [TestMethod()]
        public void getSecondIntersectionTest()
        {
            Vector center = new Vector(0, 0, 300); // TODO: Initialize to an appropriate value
            double radius = 150F; // TODO: Initialize to an appropriate value
            Sphere target = new Sphere(center, radius); // TODO: Initialize to an appropriate value
            Ray r = new Ray(new Vector(0, 0, 0), new Vector(0, 0, 1)); // TODO: Initialize to an appropriate value
            Vector expected = new Vector(0, 0, 150); // TODO: Initialize to an appropriate value
            Vector actual;
            actual = target.getFirstIntersection(r);
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.z, actual.z);
            //Assert.Inconclusive("Verify the correctness of this test method.");

            Ray reflected = target.getReflectedRay(r, actual);

            Assert.AreEqual(reflected.direction.z, -1);

            Vector center2 = new Vector(0, 0, -150);
            double radius2 = 150F;
            Sphere target2 = new Sphere(center2, radius2);

            Ray r2 = new Ray(actual, reflected.direction);
            Vector expected2 = new Vector(0, 0, 0);
            Vector actual2 = target2.getFirstIntersection(r2);
            

            Assert.AreEqual(expected2.z, actual2.z);
        }

        /// <summary>
        /// A test to determine if the reflected ray direction is correct
        /// </summary>
        /// 
        [TestMethod()]
        public void calculateReflectedVectorTest()
        {
            Vector center = new Vector(0, 0, 300); // TODO: Initialize to an appropriate value
            double radius = 150F; // TODO: Initialize to an appropriate value
            Sphere target = new Sphere(center, radius); // TODO: Initialize to an appropriate value
            Ray r = new Ray(new Vector(150*(1/Math.Sqrt(2)), 0, 0), new Vector(0, 0, 1)); // TODO: Initialize to an appropriate value
            Vector expected = new Vector(150 * (1 / Math.Sqrt(2.0)), 0, 150 + (150 * (1 / Math.Sqrt(2.0)))); // TODO: Initialize to an appropriate value
            Vector expectedReflected = new Vector(1, 0, 0);
            Vector actual;
            Ray reflected;
            actual = target.getFirstIntersection(r);
            reflected = target.getReflectedRay(r, actual);
            Assert.IsNotNull(actual);
            //Assert.AreEqual(expected.z, actual.z);
            Assert.AreEqual(expectedReflected.x, reflected.direction.x);
        }

        ///<summary>
        ///A test to determine if back side intersection is detected correctly when the starting point is inside the spere
        ///</summary>
        ///
        [TestMethod()]
        public void DetectBackFace()
        {
            Vector center = new Vector(0, 0, 300); // TODO: Initialize to an appropriate value
            double radius = 150F; // TODO: Initialize to an appropriate value
            Sphere target = new Sphere(center, radius); // TODO: Initialize to an appropriate value
            Ray r = new Ray(new Vector(0, 0, 250), new Vector(0, 0, 1)); // TODO: Initialize to an appropriate value
            Vector expected = new Vector(0,0,450); // TODO: Initialize to an appropriate value
            
            Vector actual;
            actual = target.getFirstIntersection(r);
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.z, actual.z);
        }
    }
}
