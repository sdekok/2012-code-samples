﻿using Assignment_3.Scene.Basic_Shapes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Assignment_3.GenericStructures;
using Assignment_3.Raytracer;

namespace Assignment_3_Tests
{
    
    
    /// <summary>
    ///This is a test class for TriangleTest and is intended
    ///to contain all TriangleTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TriangleTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for getFirstIntersection
        ///</summary>
        [TestMethod()]
        public void getFirstIntersectionTest()
        {
            Vector a = null; // TODO: Initialize to an appropriate value
            Vector b = null; // TODO: Initialize to an appropriate value
            Vector c = null; // TODO: Initialize to an appropriate value
            Triangle target = new Triangle(a, b, c); // TODO: Initialize to an appropriate value
            Ray r = null; // TODO: Initialize to an appropriate value
            Vector expected = null; // TODO: Initialize to an appropriate value
            Vector actual;
            actual = target.getFirstIntersection(r);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
