﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assignment_3.GenericStructures;

namespace Assignment_3.Raytracer
{
    public class Ray
    {
        public Vector origin { get; set; }
        public Vector direction { get; set; }
        public ColorVector color { get; set; } //this will be the displayed colour


        public Ray(Vector origin, Vector direction, ColorVector color = null)
        {
            this.origin = origin;
            this.direction = direction;
            if (color == null)
            {
                this.color = new ColorVector();
            }
            else
            {
                this.color = color;
            }
        }

        public static Ray operator+ (Ray lhs, Ray rhs)
        {
            lhs.color += rhs.color;

            return lhs;
        }

        public static Ray operator *(double lhs, Ray rhs)
        {
            rhs.color *= lhs;

            return rhs;
        }

        public static Ray operator *(Ray rhs, double lhs)
        {
            rhs.color *= lhs;

            return rhs;
        }
    }
}
