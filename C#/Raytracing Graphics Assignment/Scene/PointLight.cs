﻿// -----------------------------------------------------------------------
// <copyright file="PointLIght.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Assignment_3.Scene
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Assignment_3.GenericStructures;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class PointLight
    {
        public Vector origin { get; set; }
        public ColorVector color { get; set; }

        public PointLight(Vector origin, ColorVector color)
        {
            this.origin = origin;
            this.color = color;
        }
    }
}
