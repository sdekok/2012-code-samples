﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assignment_3.GenericStructures;

namespace Assignment_3.Scene.Basic_Shapes
{
    public class ShapeProperties
    {
        public double indexOfRefraction { get; set; }
        public double transparencyCoefficient { get; set; } //how transparent
        public double reflectionCoefficient { get; set; } //percentage of light that is reflected.
        public ColorVector color { get; set; }

        //need to set all the shading coefficients properly.
        public double coefficientDiffuseReflection {get; set;}
        public double coefficientSpecularReflection {get; set;}
        public double coefficientBaseLighting{ get; set;}
        public double shininessCoefficient{get; set;}
    }
}
