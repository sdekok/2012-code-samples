﻿// -----------------------------------------------------------------------
// <copyright file="Shape.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Assignment_3.Scene.Basic_Shapes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Assignment_3.GenericStructures;
    using Assignment_3.Raytracer;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Shape
    {
       ///TODO: color, shininess, material, texture, etc

        public  ShapeProperties properties { get; set; }

        public Shape()
        {
            properties = new ShapeProperties();
        }

        /// <summary>
        /// Determines if a ray intersects with the object, and if it does, it return the normal vector.  If not, it return null
        /// </summary>
        /// <param name="rayVector"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public virtual Vector getFirstIntersection(Ray r)
        {
            return new Vector();
        }
        public virtual Ray getReflectedRay(Ray original, Vector intersectionPoint)
        {
            return original;
        }
        public virtual Ray getRefractedRay(Ray original, Vector intersectionPoint, double indexOfRefractionOutside = 1.00, double indexOfRefractionInside = 1.00)
        {
            return original;
        }
        public virtual Vector calculateNormal(Vector intersectionPoint, Ray r)
        {
            return intersectionPoint;
        }
        

    }
}
