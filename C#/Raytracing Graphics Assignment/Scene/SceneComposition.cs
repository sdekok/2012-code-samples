﻿// -----------------------------------------------------------------------
// <copyright file="Scene.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Assignment_3.Scene
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Assignment_3.GenericStructures;
    using Assignment_3.Scene.Basic_Shapes;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SceneComposition
    {
        public List<Shape> shapes { get; protected set; }
        public List<PointLight> lights { get; protected set; }
        public int sceneHeight { get; set; }
        public int sceneWidth { get; set; }
        public int sceneDepth { get; set; }
        public double indexOfRefraction { get; set; }
        public double ambientLightingCoefficient { get; set; }

        public SceneComposition()
        {
            shapes = new List<Shape>();
            lights = new List<PointLight>();
        }

        public void addShape(Shape shapeToAdd)
        {
            this.shapes.Add(shapeToAdd);
        }

        public void addLight(PointLight lightPosition)
        {
            this.lights.Add(lightPosition);
        }
    }
}
