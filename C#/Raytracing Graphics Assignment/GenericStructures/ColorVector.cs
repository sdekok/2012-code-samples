﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assignment_3.GenericStructures
{
    public class ColorVector
    {
        public double red{get; set;}
        public double green { get; set; }
        public double blue { get; set; }

        public ColorVector(double red, double green, double blue)
        {
            this.red = red;
            this.green = green;
            this.blue = blue;
        }

        public ColorVector()
        {
            red = 0;
            blue = 0;
            green = 0;
        }

        public static ColorVector operator +(ColorVector lhs, ColorVector rhs)
        {
            ColorVector sum = new ColorVector();
            sum.red = lhs.red + rhs.red;
            sum.green = lhs.green + rhs.green;
            sum.blue = lhs.blue + rhs.blue;

            return sum;
        }

        public static ColorVector operator -(ColorVector lhs, ColorVector rhs)
        {
            ColorVector difference = new ColorVector();
            difference.red = lhs.red - rhs.red;
            difference.green = lhs.green - rhs.green;
            difference.blue = lhs.blue - rhs.blue;

            return difference;
        }

        public static ColorVector operator *(ColorVector lhs, double rhs)
        {
            ColorVector product = new ColorVector();
            product.red = lhs.red * rhs;
            product.green = lhs.green * rhs;
            product.blue = lhs.blue * rhs;

            return product;
        }
        public static ColorVector operator *(double lhs, ColorVector rhs)
        {
            ColorVector result = new ColorVector();

            ColorVector product = new ColorVector();
            product.red = rhs.red * lhs;
            product.green = rhs.green * lhs;
            product.blue = rhs.blue * lhs;

            return product;
        }
        public static ColorVector operator *(ColorVector lhs, ColorVector rhs)
        {
            ColorVector result = new ColorVector();

            ColorVector product = new ColorVector();
            product.red = rhs.red * lhs.red;
            product.green = rhs.green * lhs.green;
            product.blue = rhs.blue * lhs.blue;

            return product;
        }


        public static ColorVector operator /(ColorVector lhs, double rhs)
        {
            ColorVector quotient = new ColorVector();

            quotient.red = lhs.red / rhs;
            quotient.green = lhs.green / rhs;
            quotient.blue = lhs.blue / rhs;

            return quotient;
        }

        
        
    }
}
