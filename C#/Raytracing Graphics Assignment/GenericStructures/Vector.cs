﻿// -----------------------------------------------------------------------
// <copyright file="Vertex.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Assignment_3.GenericStructures
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Custom Vertex Class to store the point data of each shape
    /// </summary>
    public class Vector
    {
        public double x {get; set;}
        public double y {get; set;}
        public double z {get; set;}

        public Vector(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector()
        {
            this.x = 0;
            this.y = 0;
            this.z = 1;
        }

        public static Vector CrossProduct(Vector a, Vector b)
        {
            Vector product = new Vector();
            product.x = (a.y*b.z) - (a.z*b.y);
            product.y = (a.z * b.x) - (a.x * b.z);
            product.z = (a.x * b.y) - (a.y * b.x);

            return product;
        }

        public static double DotProduct(Vector a, Vector b)
        {
            double dotProduct = (a.x*b.x)+(a.y*b.y)+(a.z*b.z);

            return dotProduct;
        }
        
        public double DotProduct(Vector other)
        {
            double dotProduct = (this.x * other.x) + (this.y * other.y) + (this.z * other.z);

            return dotProduct;
        }
        public double Length()
        {
            double toReturn = Math.Sqrt(Math.Pow(this.x, 2) + Math.Pow(this.y, 2) + Math.Pow(this.z, 2));
            
            return toReturn;
        }

        public static Vector operator +(Vector lhs, Vector rhs)
        {
            Vector sum = new Vector();
            sum.x = lhs.x + rhs.x;
            sum.y = lhs.y + rhs.y;
            sum.z = lhs.z + rhs.z;

            return sum;
        }

        public static Vector operator -(Vector lhs, Vector rhs)
        {
            Vector difference = new Vector();
            difference.x = lhs.x - rhs.x;
            difference.y = lhs.y - rhs.y;
            difference.z = lhs.z - rhs.z;

            return difference;
        }

        public static Vector operator *(Vector lhs, double rhs)
        {
            Vector result = new Vector();

            result.x = lhs.x*rhs;
            result.y = lhs.y*rhs;
            result.z = lhs.z*rhs;

            return result;
        }
        public static Vector operator *(double lhs, Vector rhs)
        {
            Vector result = new Vector();

            result.x = rhs.x*lhs;
            result.y = rhs.y*lhs;
            result.z = rhs.z*lhs;

            return result;
        }

        public static Vector operator /(Vector lhs, double rhs)
        {
            Vector result = new Vector();

            result.x = lhs.x / rhs;
            result.y = lhs.y / rhs;
            result.z = lhs.z / rhs;

            return result;
        }

        public static Vector operator /(Vector lhs, Vector rhs)
        {
            Vector result = new Vector();

            result.x = lhs.x / rhs.x;
            result.y = lhs.y / rhs.y;
            result.z = lhs.z / rhs.z;

            return result;
        }

        public bool equals(Vector rhs)
        {
            if (rhs == null)
            {
                return false;
            }
            if (this.x == rhs.x && this.y == rhs.y && this.z == rhs.z)
            {
                return true;
            }
            return false;
        }
        
    }
}
