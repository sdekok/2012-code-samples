﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Assignment_3
{
    /// <summary>
    /// Interaction logic for RenderToFile.xaml
    /// </summary>
    public partial class RenderToFile : Window
    {
        public RenderToFile()
        {
            InitializeComponent();
        }

        private void _continue_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
    }
}
