#include "Point3f.h"


Point3f::Point3f( double x, double y, double z ):x_(x),y_(y),z_(z)
{

}

Point3f::Point3f( double x, double y ):x_(x),y_(y),z_(0)
{

}

Point3f::Point3f():x_(0), y_(0), z_(0)
{
	
}

Point3f::~Point3f(void)
{
}

double Point3f::x() const
{
	return x_;
}

void Point3f::x( const double &value )
{
	x_=value;
}

double Point3f::y() const
{
	return y_;
}

void Point3f::y( const double &value )
{
	y_ = value;
}

double Point3f::z() const
{
	return z_;
}

void Point3f::z( const double &value )
{
	z_ = value;
}

Point3f Point3f::operator*( const double &other )
{
	return Point3f(x_*other,y_*other,z_*other);
}

Point3f Point3f::operator*( Point3f &other )
{
	Point3f temp(x_*other.x(),y_*other.y(),z_*other.z());
	return temp;
}

Point3f Point3f::operator*( const int &other )
{
	return this->operator*( (double) other);
}

Point3f Point3f::operator+( Point3f &other )
{
	Point3f temp(other.x() + this->x_,other.y() + this->y_,other.z() + this->z_);
	return temp;
}

Point3f operator*( const double &lhs, const Point3f &rhs )
{
	return Point3f(rhs.x_*lhs,rhs.y_*lhs,rhs.z_*lhs);
}

QDataStream & operator<<( QDataStream &out, const Point3f &point )
{
	out << point.x() << point.y() << point.z();
	return out;
}

QDataStream & operator>>( QDataStream &in, Point3f &point )
{
	double x, y, z;

	in >> x;
	in >> y;
	in >> z;


	//Shape *shape2;// = (Shape *)new Line();


	//shape = ;

	//Circle::Circle();

	point.x(x);
	point.y(y);
	point.z(z);

	return in;

}