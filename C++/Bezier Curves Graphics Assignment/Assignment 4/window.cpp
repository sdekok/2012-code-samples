#include "Window.h"

Window::Window(QWidget *parent) : QMainWindow(parent) {
	_widget = new OpenGLWidget(this);					//	Initialize the GL Widget
	setCentralWidget(_widget);						//	This is the main part of the interface, so set it as the central widget of the window
	_widget->setFocus();								//	Set focus to capture keyboard and mouse events
	//_widget->setMouseTracking(true);				//Capture mouse movements when mouse button is not pressed.

	setupStatusBar();

	setupMenus();

	setupDocks();
	
	//AddShapeButtons();

	SetupLineTypes();

	SetupOptions();

	//AddColourWidgets();

	setWindowTitle("Graphics Assignment 4");						//	Set the title of the window to be "Graphics Assignment 1"

}

Window::~Window() { }

void Window::setColor( QColor toSet )
{
	_currentColor = &toSet;

	//Set Combo's

	_colorLabel->setPalette(QPalette(toSet));
	//_displayCurrentColor->setPalette(QPalette(toSet));
}

void Window::AddColourWidgets()
{

	_currentColor = new QColor(0,0,0);
	_colorDialog = new QColorDialog(this);
	//	_colorDialog->setOption(QColorDialog.ShowAlphaChannel, true);
	_colorDialog->setCurrentColor(*_currentColor);
	_colorDialog->setOption(_colorDialog->ShowAlphaChannel, true);

	connect(_colorDialog, SIGNAL(colorSelected (QColor)), this, SLOT(setColor(QColor)));
	connect(_colorDialog, SIGNAL(colorSelected (QColor)), _widget, SLOT(setColor(QColor)));
	connect(this, SIGNAL(colorChanged(QColor)), _widget, SLOT(setColor(QColor)));

	//_displayCurrentColor = new QLabel(this);

	QPushButton* selectColor = new QPushButton("Choose Colour");
	selectColor->setToolTip("Select a colour for the next shape you draw.  The new colour will not affect existing shapes");

	connect(selectColor, SIGNAL(clicked()), _colorDialog, SLOT(open()));

	_colorLabel = new QLabel();
	_colorLabel->setPalette(QPalette(*_currentColor));
	_colorLabel->setAutoFillBackground(true);
	_colorLabel->setText("");

	QFrame* colorFrame = new QFrame();
	QGridLayout* colorLayout = new QGridLayout();
	colorFrame->setLayout(colorLayout);

	colorLayout->addWidget(selectColor, 0, 0);
	colorLayout->addWidget(_colorLabel, 1, 0);
	colorLayout->addWidget(new QLabel(),2, 0, 3,1);

	_dockOptions->setWidget(colorFrame);
}



void Window::AddShapeButtons()
{
	_buttonLine = new QPushButton("Line");		//	Create three push buttons. These are currently not connected to anything. see the 'connect' function as in the status bar for reference
	_buttonLine->setToolTip("Line Tool - draw a line");
	_buttonPolygon = new QPushButton("Polygon");
	_buttonPolygon->setToolTip("Polygon Tool - draw a polygon");
	_buttonCircle = new QPushButton("Circle");
	_buttonCircle->setToolTip("Circle Tool - draw a circle");

	connect(_buttonLine, SIGNAL(clicked()), _widget, SLOT(buttonLinePressed()));
	connect(_buttonPolygon, SIGNAL(clicked()),_widget, SLOT(buttonPolygonPressed()));
	connect(_buttonCircle, SIGNAL(clicked()),_widget, SLOT(buttonCirclePressed()));

	QLabel* spacer = new QLabel();

	QFrame* frame1 = new QFrame();					//	A QFrame is a widget that can hold a layout of other widgets
	//QVBoxLayout* layout = new QVBoxLayout();		//	A vbox layout can arrange widgets in a vertical column
	//QHBoxLayout* layout = new QHBoxLayout();		//	An hbox layout can arrange widgets in a horizontal row
	QGridLayout* layout = new QGridLayout();		//	A grid layout can arrange widgets in a grid
	frame1->setLayout(layout);						//	Attach the layout to the frame
	layout->addWidget(_buttonLine, 0, 0);				//	Add one button to 0,0 in the grid layout. HBox and VBox layouts to not take the coordinate arguments
	layout->addWidget(_buttonPolygon, 1, 0);			//	Add one button to 1,0 in the grid, and make it 1 row high and 2 columns wide
	layout->addWidget(_buttonCircle, 2, 0);
	layout->addWidget(spacer, 3, 0);
	//	Add the last button to 0,1 in the grid
	_dockTools->setWidget(frame1);

	//	Finally, set the frame as the dock's widget
	// Frames are very useful. You could also use a frame as your central widget if you want more than one widget to appear in the center of the window
	// Other QObjects, such as QScrollAreas and QToolBoxes can be used in a similar fashion
}

void Window::setupDocks()
{
	_dockTools = new QDockWidget("Tools", this);			//	Create three new dock widgets. These are movable panels that can be placed around the edges of the central widget
	_dockOptions = new QDockWidget("Options", this);		
	//_dockOther = new QDockWidget("Other", this);
	addDockWidget(Qt::LeftDockWidgetArea, _dockTools);	//	Place two dock widgets on the right side of the window
	addDockWidget(Qt::RightDockWidgetArea, _dockOptions);

	_dockTools->setFeatures(_dockTools->NoDockWidgetFeatures);
	_dockOptions->setFeatures(_dockOptions->NoDockWidgetFeatures);
}

void Window::setupMenus()
{
	menuBar = new QMenuBar();						//	Create a new menu bar
	fileMenu = new QMenu("&File", this);			//	Create a new menu called "File" the '&' in "&File" automatically creates a shortcut to the menu (ALT+F)
	// Add an action called "Quit" to the menu. It calls the 'close' function on the window when activated, and has the shortcut (CTRL+Q)
	fileMenu->addAction("&New", _widget, SLOT(newCanvas()), QKeySequence("CTRL+N"));
	fileMenu->addAction("&Open", _widget, SLOT(open()), QKeySequence("CTRL+O"));
	fileMenu->addAction("&Save", _widget, SLOT(save()), QKeySequence("CTRL+S"));
	fileMenu->addAction("Save As", _widget, SLOT(saveAs()));
	fileMenu->addAction("&Quit", this, SLOT(close()), QKeySequence("CTRL+Q"));

	//editMenu = new QMenu("&Edit", this);
	//editMenu->addAction("&Undo", _widget, SLOT(undo()), QKeySequence("CTRL+Z"));
	//editMenu->addAction("&Redo", _widget, SLOT(redo()), QKeySequence("CTRL+Y"));

	menuBar->addMenu(fileMenu);						//	Put the file menu on the menu bar
	//menuBar->addMenu(editMenu);
	setMenuBar(menuBar);							//	Set the window's menu bar to be the one we just created


}

void Window::setupStatusBar()
{
	statusBar = new QStatusBar(this);				//	Create a status bar
	statusBar->showMessage("Click on a curve type to see the approximation");					//	Set a default message that is displayed on the status bar at startup
	//	Connect the GL widget's 'showMessage' signal to the status bar's 'showMessage' function so that a message will be displayed whenever the GL widget sends the signal
	connect(_widget, SIGNAL(showMessage(QString)), statusBar, SLOT(showMessage(QString)));
	setStatusBar(statusBar);
}

void Window::SetupLineTypes()
{
	casteljau = new QPushButton("CastelJau");
	quadratic_besier = new QPushButton("Quadratic Besier");
	cubic_besier = new QPushButton("Cubic Besier");
	Closed_Bspline = new QPushButton("Closed Quadratic Bspline");
	Open_Bspline = new QPushButton("Open Quadratic Bspline");
	Closed_Cubic_Bspline = new QPushButton("Closed Cubic Bspline");
	Open_Cubic_Bspline = new QPushButton("Open Cubic Bspline");


	connect(casteljau, SIGNAL(clicked()), _widget, SLOT(setCasteljau()));
	connect(quadratic_besier, SIGNAL(clicked()), _widget, SLOT(setQuadratic_Besier()));
	connect(cubic_besier, SIGNAL(clicked()), _widget, SLOT(setCubic_Besier()));
	connect(Closed_Bspline, SIGNAL(clicked()), _widget, SLOT(setClosed_BSpline()));
	connect(Open_Bspline, SIGNAL(clicked()), _widget, SLOT(setOpen_BSpline()));
	connect(Closed_Cubic_Bspline, SIGNAL(clicked()), _widget, SLOT(setClosed_Cubic_BSpline()));
	connect(Open_Cubic_Bspline, SIGNAL(clicked()), _widget, SLOT(setOpen_Cubic_BSpline()));

	QLabel* spacer = new QLabel();

	QFrame* frame1 = new QFrame();					//	A QFrame is a widget that can hold a layout of other widgets
	//QVBoxLayout* layout = new QVBoxLayout();		//	A vbox layout can arrange widgets in a vertical column
	//QHBoxLayout* layout = new QHBoxLayout();		//	An hbox layout can arrange widgets in a horizontal row
	QGridLayout* layout = new QGridLayout();		//	A grid layout can arrange widgets in a grid
	frame1->setLayout(layout);						//	Attach the layout to the frame
	layout->addWidget(casteljau, 0, 0);				//	Add one button to 0,0 in the grid layout. HBox and VBox layouts to not take the coordinate arguments
	layout->addWidget(quadratic_besier, 1, 0);			//	Add one button to 1,0 in the grid, and make it 1 row high and 2 columns wide
	layout->addWidget(cubic_besier, 2, 0);
	layout->addWidget(Closed_Bspline, 3, 0);
	layout->addWidget(Open_Bspline, 4, 0);
	layout->addWidget(Closed_Cubic_Bspline, 5, 0);
	layout->addWidget(Open_Cubic_Bspline, 6, 0);
	layout->addWidget(spacer, 7, 0);
	//	Add the last button to 0,1 in the grid
	_dockTools->setWidget(frame1);

}

void Window::SetupOptions()
{
	QLabel* zLabel = new QLabel();
	zLabel->setText("Z Value");
	z = new QSpinBox();
	z->setValue(50);
	
	QLabel* subDivisionLabel = new QLabel();
	subDivisionLabel->setText("# of Sub Divisions");
	subDivision_Count = new QSpinBox();
	subDivision_Count->setValue(10);

	viewZ = new QPushButton("Toggle Z view");

	connect(z, SIGNAL(valueChanged(int)), _widget, SLOT(setZ(int)));
	connect(subDivision_Count, SIGNAL(valueChanged(int)), _widget, SLOT(setSubDivisionCount(int)));
	connect(viewZ, SIGNAL(clicked()), _widget, SLOT(toggleViewZ()));

	QLabel* spacer = new QLabel();

	QFrame* frame1 = new QFrame();					//	A QFrame is a widget that can hold a layout of other widgets
	//QVBoxLayout* layout = new QVBoxLayout();		//	A vbox layout can arrange widgets in a vertical column
	//QHBoxLayout* layout = new QHBoxLayout();		//	An hbox layout can arrange widgets in a horizontal row
	QGridLayout* layout = new QGridLayout();		//	A grid layout can arrange widgets in a grid
	frame1->setLayout(layout);//	Attach the layout to the frame
	layout->addWidget(zLabel,0,0);
	layout->addWidget(z, 1, 0);	
	layout->addWidget(subDivisionLabel,2,0);
	layout->addWidget(subDivision_Count, 3, 0);	
	layout->addWidget(viewZ,4,0);
	layout->addWidget(spacer, 5, 0,10,1);

	_dockOptions->setWidget(frame1);


}
