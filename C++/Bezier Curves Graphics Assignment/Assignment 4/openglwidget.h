#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#include <QObject>
#include <QGLWidget>
#include "Point3f.h"
#include<QKeyEvent>
#include<QMouseEvent>
#include <gl/GLU.h>

class OpenGLWidget : public QGLWidget
{
	Q_OBJECT

public:
	OpenGLWidget(QWidget *parent);
	~OpenGLWidget();

	enum lineType{CASTELJAU,QUADRATIC_BESIER, CUBIC_BESIER, CLOSED_BSPLINE, OPEN_BSPLINE, CLOSED_CUBIC_BSPLINE, OPEN_CUBIC_BSPLINE};

signals:

public slots:

	//set Line Types
	void setLineType(lineType typeOfLine);
	void setCasteljau();
	void setQuadratic_Besier();
	void setCubic_Besier();
	void setClosed_BSpline();
	void setOpen_BSpline();
	void setClosed_Cubic_BSpline();
	void setOpen_Cubic_BSpline();
	void setSubDivisionCount(int count);
	
	void setZ(int toSet);

	void toggleViewZ();

	//file management
	void save();
	void saveAs();
	void newCanvas();
	void open();

private:
	QList<Point3f> *points_;
	QList<Point3f> *subdivisionPoints_; 

	lineType typeOfLine_;
	bool isClosed;
	bool viewZ;

	double subdivision_;

	//size variables
	float _width, _height;
	float _xRatio, _yRatio;
	float _scale;
	float _x, _y, _z;
	int pointToEdit;

	//file saving info
	bool fileSaved;
	QFile saveLocation;

	//calculate different curves
	void draw();
	void calculateCasteljau();
	//r = degree
	//i = start current starting point (for subdivision)
	//t = point to evaluate from start to finish (between 0 and 1)
	Point3f getCasteljauPoint(const int &r, const int &i, const double &t);
	
	void calculateQuadraticBesier();
	void calculateCubicBesier();
	void calculateClosedBSpline();
	void calculateOpenBspline();
	void calculateClosedCubicBSpline();
	void calculateOpenCubicBspline();

	//display the curves
	void initializeGL();
	void resizeGL(int width, int height);
	void paintGL();
	void drawPoints();
	void drawCurve();

	//add, remove, and edit points
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);

	void setXY();
	int findPoint(double x, double y);

};

#endif // OPENGLWIDGET_H
