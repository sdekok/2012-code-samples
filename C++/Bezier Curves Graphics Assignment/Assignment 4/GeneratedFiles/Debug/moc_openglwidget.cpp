/****************************************************************************
** Meta object code from reading C++ file 'openglwidget.h'
**
** Created: Fri Apr 13 12:46:37 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../openglwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'openglwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OpenGLWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   14,   13,   13, 0x0a,
      47,   13,   13,   13, 0x0a,
      62,   13,   13,   13, 0x0a,
      84,   13,   13,   13, 0x0a,
     102,   13,   13,   13, 0x0a,
     122,   13,   13,   13, 0x0a,
     140,   13,   13,   13, 0x0a,
     166,   13,   13,   13, 0x0a,
     196,  190,   13,   13, 0x0a,
     227,  221,   13,   13, 0x0a,
     237,   13,   13,   13, 0x0a,
     251,   13,   13,   13, 0x0a,
     258,   13,   13,   13, 0x0a,
     267,   13,   13,   13, 0x0a,
     279,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_OpenGLWidget[] = {
    "OpenGLWidget\0\0typeOfLine\0setLineType(lineType)\0"
    "setCasteljau()\0setQuadratic_Besier()\0"
    "setCubic_Besier()\0setClosed_BSpline()\0"
    "setOpen_BSpline()\0setClosed_Cubic_BSpline()\0"
    "setOpen_Cubic_BSpline()\0count\0"
    "setSubDivisionCount(int)\0toSet\0setZ(int)\0"
    "toggleViewZ()\0save()\0saveAs()\0newCanvas()\0"
    "open()\0"
};

void OpenGLWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OpenGLWidget *_t = static_cast<OpenGLWidget *>(_o);
        switch (_id) {
        case 0: _t->setLineType((*reinterpret_cast< lineType(*)>(_a[1]))); break;
        case 1: _t->setCasteljau(); break;
        case 2: _t->setQuadratic_Besier(); break;
        case 3: _t->setCubic_Besier(); break;
        case 4: _t->setClosed_BSpline(); break;
        case 5: _t->setOpen_BSpline(); break;
        case 6: _t->setClosed_Cubic_BSpline(); break;
        case 7: _t->setOpen_Cubic_BSpline(); break;
        case 8: _t->setSubDivisionCount((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->setZ((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->toggleViewZ(); break;
        case 11: _t->save(); break;
        case 12: _t->saveAs(); break;
        case 13: _t->newCanvas(); break;
        case 14: _t->open(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData OpenGLWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OpenGLWidget::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_OpenGLWidget,
      qt_meta_data_OpenGLWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OpenGLWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OpenGLWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OpenGLWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OpenGLWidget))
        return static_cast<void*>(const_cast< OpenGLWidget*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int OpenGLWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
