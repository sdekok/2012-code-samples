#pragma once

#include "..\src\corelib\io\qdatastream.h"
//using namespace std;

class Point3f
{
public:
	Point3f(double x, double y, double z);
	Point3f(double x, double y);
	Point3f();
	~Point3f();

	double x() const;
	void x(const double &value);
	double y() const;
	void y(const double &value);
	double z() const;
	void z(const double &value);

	//operator overloads
	Point3f operator *(const double &other);
	Point3f operator *(const int &other);
	Point3f operator *(Point3f &other);
	friend Point3f operator*(const double &lhs, const Point3f &rhs);
	Point3f operator+(Point3f &other);

protected:
	double x_,y_,z_;



};

QDataStream &operator<<(QDataStream &out, const Point3f &point);
QDataStream &operator>>(QDataStream &in, Point3f &point);
