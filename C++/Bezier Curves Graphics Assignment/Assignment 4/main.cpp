//#include "assignment4.h"
#include "window.h"
#include <QtGui/QApplication>

int main(int argc, char *argv[])
{
	QApplication app (argc, argv);		//	Create an instance of QApplication

	Window* w = new Window();			//	Create an instance of Window
	w->resize(800, 600);				//	Set the window to 500x500
	w->show();							//	Show the window

	return app.exec();					//	Pass control to QT
}
