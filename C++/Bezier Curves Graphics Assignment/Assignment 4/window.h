#pragma once
#include <QWidget>
#include <QtGui>
#include "OpenGLWidget.h"
#include <QColorDialog>

class Window : public QMainWindow {
	Q_OBJECT

public:
	Window(QWidget *parent = NULL);

	~Window();
signals:
	void colorChanged(QColor color);

	public slots:

		private slots:
			void setColor(QColor toSet);

private:

	OpenGLWidget* _widget;

	void setupMenus();
	QMenuBar* menuBar;
	QMenu* fileMenu;
	QMenu* editMenu;

	void setupStatusBar();
	QStatusBar* statusBar;

	void setupDocks();
	QDockWidget* _dockTools;
	QDockWidget* _dockOptions;
	QDockWidget* _dockOther;

	void AddColourWidgets();
	QColor* _currentColor;
	QColorDialog* _colorDialog;
	QLabel* _colorLabel;

	void AddShapeButtons();
	QPushButton* _buttonLine;
	QPushButton* _buttonPolygon;
	QPushButton* _buttonCircle;

	void SetupLineTypes();
	QPushButton *casteljau;
	QPushButton *quadratic_besier;
	QPushButton *cubic_besier;
	QPushButton *Closed_Bspline;
	QPushButton *Open_Bspline;
	QPushButton *Closed_Cubic_Bspline;
	QPushButton *Open_Cubic_Bspline;

	void SetupOptions();
	QSpinBox *z;
	QSpinBox *subDivision_Count;
	QPushButton* viewZ;

	
};
